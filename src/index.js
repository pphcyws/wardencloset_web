import React from 'react';
import ReactDOM from 'react-dom';
import { createBrowserHistory } from "history";

import { Router, Route, Switch  } from "react-router-dom";
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import reducers from './reducers'

import { ThemeProvider } from '@mui/material/styles';
import { createTheme } from '@mui/material/styles';
// import './index.css';
// import App from './App';

// import PageNotFoundLayout from "./pages/PageNotFound";
import Main from "./pages/Main";
import Mint from "./pages/Mint";


// import reportWebVitals from './reportWebVitals';

const store = createStore(
  reducers,
  applyMiddleware(thunk)
)
const hist = createBrowserHistory();

const theme = createTheme({
  components: {
    MuiTypography: {
      defaultProps: {
        fontFamily: 'Prompt',
      },
    },
    MuiListItemText: {
      defaultProps: {
        fontFamily: 'Prompt',
      },
    },
    MuiButton: {
      styleOverrides: {
        contained: { 
          fontFamily: "Prompt"
        },
      }
    }
  },
  typography: {
    fontFamily: [
      'Prompt'
    ].join(','),
  },
  palette: {
    primary: { 
      main:"#202251"
    },
    secondary: {
      light: '#ff0000',
      main: '#ffffff',
      // dark: will be calculated from palette.secondary.main,
      contrastText: '#202251',
    },
  },
});


ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Provider store={store}>
      <Router history={hist}>
        <Switch>
          <Route exact path="/" component={ Main } />
          <Route exact path="/mint" component={ Mint } />
          <Route component={ Main } />
        </Switch>
      </Router>
    </Provider>
  </ThemeProvider>
  ,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
