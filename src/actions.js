import { API_HOST } from "./constants"

export const uploadBody = (formData) => { 
    let url = `${API_HOST}/rest/v2`
    // console.log(url)
    return (dispatch) => {
        fetch(url,  { method: 'POST', body: formData })
            .then(res => {
                if (res.ok){
                    return res
                }
                throw Error (res.status)
            })
            .then(result =>  result.json())
            .then(result => {
                setTimeout(() => {
                    dispatch({
                        type: 'UPLOADBODY_OK',
                        payload: result
                    })
                },2)

            })
            .catch(e => 
                dispatch({
                    type: 'UPLOADBODY_FAILED',
                    payload: e.message
                })
            )
    }
}
