import { combineReducers } from 'redux'



let uploadBodyInit = {isFailed : false, isLoading: false, data: null }
function uploadBody (state = uploadBodyInit, action){
    switch (action.type){
        case "UPLOADBODY_OK": 
            uploadBodyInit = {...state, isFailed: false, isLoading: false, data: action.payload }
            return uploadBodyInit
        case "UPLOADBODY_FAILED":
            uploadBodyInit = {...state, isFailed: true, isLoading: false, data: action.payload }
            return uploadBodyInit
        case "UPLOADBODY_RESET": 
            uploadBodyInit = {...state, isFailed: false, isLoading: false, data: null }
            return uploadBodyInit
        default: 
            return uploadBodyInit
    }
}

const reducers = combineReducers({
    uploadBody: uploadBody,

 
})

export default reducers