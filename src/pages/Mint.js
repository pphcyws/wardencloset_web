import React, { Component } from 'react'
import * as actions from '../actions'
import { bindActionCreators } from 'redux'; 
import {connect} from 'react-redux'
import Web3 from 'web3';
import Button from '@mui/material/Button';
// import { styled } from '@mui/material/styles';
import Grid from '@mui/material/Grid';

import Timeline from '@mui/lab/Timeline';
import TimelineItem from '@mui/lab/TimelineItem';
import TimelineSeparator from '@mui/lab/TimelineSeparator';
import TimelineConnector from '@mui/lab/TimelineConnector';
import TimelineContent from '@mui/lab/TimelineContent';
import TimelineOppositeContent from '@mui/lab/TimelineOppositeContent';
import TimelineDot from '@mui/lab/TimelineDot';

import PlayArrowOutlinedIcon from '@mui/icons-material/PlayArrowOutlined';
import RedeemIcon from '@mui/icons-material/Redeem';
import LaptopMacIcon from '@mui/icons-material/LaptopMac';
import CodeIcon from '@mui/icons-material/Code';
import MoreHorizIcon from '@mui/icons-material/MoreHoriz';
import PeopleIcon from '@mui/icons-material/People';
import ThumbUpIcon from '@mui/icons-material/ThumbUp';
import AutoAwesomeIcon from '@mui/icons-material/AutoAwesome';
import PhonelinkIcon from '@mui/icons-material/Phonelink';
import Typography from '@mui/material/Typography';
import './Mint.css'

let contractAddress = "0x9FE48597d54E80bE4303965483dFB5f58f1e7F9f"
let abi = [
	{
		"inputs": [],
		"stateMutability": "nonpayable",
		"type": "constructor"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": true,
				"internalType": "address",
				"name": "_owner",
				"type": "address"
			},
			{
				"indexed": true,
				"internalType": "address",
				"name": "_approved",
				"type": "address"
			},
			{
				"indexed": true,
				"internalType": "uint256",
				"name": "_tokenId",
				"type": "uint256"
			}
		],
		"name": "Approval",
		"type": "event"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": true,
				"internalType": "address",
				"name": "_owner",
				"type": "address"
			},
			{
				"indexed": true,
				"internalType": "address",
				"name": "_operator",
				"type": "address"
			},
			{
				"indexed": false,
				"internalType": "bool",
				"name": "_approved",
				"type": "bool"
			}
		],
		"name": "ApprovalForAll",
		"type": "event"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": true,
				"internalType": "address",
				"name": "previousOwner",
				"type": "address"
			},
			{
				"indexed": true,
				"internalType": "address",
				"name": "newOwner",
				"type": "address"
			}
		],
		"name": "OwnershipTransferred",
		"type": "event"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": true,
				"internalType": "address",
				"name": "_from",
				"type": "address"
			},
			{
				"indexed": true,
				"internalType": "address",
				"name": "_to",
				"type": "address"
			},
			{
				"indexed": true,
				"internalType": "uint256",
				"name": "_tokenId",
				"type": "uint256"
			}
		],
		"name": "Transfer",
		"type": "event"
	},
	{
		"inputs": [],
		"name": "CANNOT_TRANSFER_TO_ZERO_ADDRESS",
		"outputs": [
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [],
		"name": "NOT_CURRENT_OWNER",
		"outputs": [
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "address",
				"name": "_approved",
				"type": "address"
			},
			{
				"internalType": "uint256",
				"name": "_tokenId",
				"type": "uint256"
			}
		],
		"name": "approve",
		"outputs": [],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "address",
				"name": "_owner",
				"type": "address"
			}
		],
		"name": "balanceOf",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "_tokenId",
				"type": "uint256"
			}
		],
		"name": "getApproved",
		"outputs": [
			{
				"internalType": "address",
				"name": "",
				"type": "address"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "address",
				"name": "_owner",
				"type": "address"
			},
			{
				"internalType": "address",
				"name": "_operator",
				"type": "address"
			}
		],
		"name": "isApprovedForAll",
		"outputs": [
			{
				"internalType": "bool",
				"name": "",
				"type": "bool"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "address",
				"name": "_to",
				"type": "address"
			},
			{
				"internalType": "uint256",
				"name": "_tokenId",
				"type": "uint256"
			},
			{
				"internalType": "string",
				"name": "_uri",
				"type": "string"
			}
		],
		"name": "mint",
		"outputs": [],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [],
		"name": "name",
		"outputs": [
			{
				"internalType": "string",
				"name": "_name",
				"type": "string"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [],
		"name": "owner",
		"outputs": [
			{
				"internalType": "address",
				"name": "",
				"type": "address"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "_tokenId",
				"type": "uint256"
			}
		],
		"name": "ownerOf",
		"outputs": [
			{
				"internalType": "address",
				"name": "_owner",
				"type": "address"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "address",
				"name": "_from",
				"type": "address"
			},
			{
				"internalType": "address",
				"name": "_to",
				"type": "address"
			},
			{
				"internalType": "uint256",
				"name": "_tokenId",
				"type": "uint256"
			}
		],
		"name": "safeTransferFrom",
		"outputs": [],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "address",
				"name": "_from",
				"type": "address"
			},
			{
				"internalType": "address",
				"name": "_to",
				"type": "address"
			},
			{
				"internalType": "uint256",
				"name": "_tokenId",
				"type": "uint256"
			},
			{
				"internalType": "bytes",
				"name": "_data",
				"type": "bytes"
			}
		],
		"name": "safeTransferFrom",
		"outputs": [],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "address",
				"name": "_operator",
				"type": "address"
			},
			{
				"internalType": "bool",
				"name": "_approved",
				"type": "bool"
			}
		],
		"name": "setApprovalForAll",
		"outputs": [],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "bytes4",
				"name": "_interfaceID",
				"type": "bytes4"
			}
		],
		"name": "supportsInterface",
		"outputs": [
			{
				"internalType": "bool",
				"name": "",
				"type": "bool"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [],
		"name": "symbol",
		"outputs": [
			{
				"internalType": "string",
				"name": "_symbol",
				"type": "string"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "_tokenId",
				"type": "uint256"
			}
		],
		"name": "tokenURI",
		"outputs": [
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "address",
				"name": "_from",
				"type": "address"
			},
			{
				"internalType": "address",
				"name": "_to",
				"type": "address"
			},
			{
				"internalType": "uint256",
				"name": "_tokenId",
				"type": "uint256"
			}
		],
		"name": "transferFrom",
		"outputs": [],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "address",
				"name": "_newOwner",
				"type": "address"
			}
		],
		"name": "transferOwnership",
		"outputs": [],
		"stateMutability": "nonpayable",
		"type": "function"
	}
];


let web3 = null;
class Mint extends Component {
    state = {
		numberOfMinted : 0,
		isTimeToMint: false, //run on componentDidMount & everytime click mint, check on server 
    }

    componentDidMount(){
        console.log("componentDidMount")

    }

    onClickConnectWalletBtn = () => {
        if (window.ethereum) {
            web3 = new Web3(window.ethereum);

            try { 
              // Request account access if needed
              window.ethereum.enable().then(result => {
                // Now you can start your app & access web3 freely:
                // startApp()
                console.log(result);

                let contract = new web3.eth.Contract(abi, contractAddress)
                contract.methods.owner().call().then( addr => { 
                    console.log("addr : ",addr);
                    
                } );
              })
            }
            catch(err) {
              console.log(err);
            }
        }
        // Legacy dapp browsers, checking if Web3 has been injected by the browser (Mist/MetaMask)
        else if (typeof web3 !== 'undefined' && web3 !== null) {
            // Use Mist/MetaMask's provider
            web3 = new Web3(web3.currentProvider);

            // Now you can start your app & access web3 freely:
            console.log("Now you can start your app & access web3 freely");

        } 
        else {
            console.log('No web3? You should consider trying MetaMask!')
        }

    }

	BasicTimeline = () => { 
		return (
			<Timeline position="alternate">
			<TimelineItem>
			  <TimelineOppositeContent
				sx={{ m: 'auto 0' }}
				variant="body2"
				color="text.secondary"
			  >
				24 January 2022
			  </TimelineOppositeContent>
			  <TimelineSeparator>
				<TimelineConnector />
				<TimelineDot color="primary">
				  <PlayArrowOutlinedIcon />
				</TimelineDot>
				<TimelineConnector />
			  </TimelineSeparator>
			  <TimelineContent sx={{ py: '32px', px: 2 }}>
				<Typography variant="h6" component="span">
					Open Warden Closet
				</Typography>
				<Typography>The 1<sup>st</sup> version, MWAD can change clothing</Typography>
			  </TimelineContent>
			</TimelineItem>

			<TimelineItem>
			  <TimelineOppositeContent
				sx={{ m: 'auto 0' }}
				variant="body2"
				color="text.secondary"
			  >
				7 February 2022
			  </TimelineOppositeContent>
			  <TimelineSeparator>
				<TimelineConnector />
				<TimelineDot color="primary">
				  <ThumbUpIcon />
				</TimelineDot>
				<TimelineConnector />
			  </TimelineSeparator>
			  <TimelineContent sx={{ py: '32px', px: 2 }}>
				<Typography variant="h6" component="span">
					Big Upgrade on Warden Closet
				</Typography>
				<Typography>The 2<sup>nd</sup> version, Upgrade many feature</Typography>
			  </TimelineContent>
			</TimelineItem>

			<TimelineItem>
			  <TimelineOppositeContent
				sx={{ m: 'auto 0' }}
				variant="body2"
				color="text.secondary"
			  >
				8 February 2022
			  </TimelineOppositeContent>
			  <TimelineSeparator>
				<TimelineConnector />
				<TimelineDot color="primary">
				  <CodeIcon />
				</TimelineDot>
				<TimelineConnector />
			  </TimelineSeparator>
			  <TimelineContent sx={{ py: '32px', px: 2 }}>
				<Typography variant="h6" component="span">
					Public Source Code
				</Typography>
				<Typography>https://gitlab.com/pphcyws</Typography>
			  </TimelineContent>
			</TimelineItem>

			<TimelineItem>
			  <TimelineOppositeContent
				sx={{ m: 'auto 0' }}
				variant="body2"
				color="text.secondary"
			  >
				19 February 2022
			  </TimelineOppositeContent>
			  <TimelineSeparator>
				<TimelineConnector />
				<TimelineDot color="primary">
				  <PeopleIcon />
				</TimelineDot>
				<TimelineConnector />
			  </TimelineSeparator>
			  <TimelineContent sx={{ py: '32px', px: 2 }}>
				<Typography variant="h6" component="span">
					Warden Closet X Atevada (1<sup>st</sup> collaboration)
				</Typography>
				<Typography>MWAD can ware Atevada mask</Typography>
			  </TimelineContent>
			</TimelineItem>

			<TimelineItem>
			  <TimelineOppositeContent
				sx={{ m: 'auto 0' }}
				variant="body2"
				color="text.secondary"
			  >
				2 March 2022
			  </TimelineOppositeContent>
			  <TimelineSeparator>
				<TimelineConnector />
				<TimelineDot color="primary">
				  <PeopleIcon />
				</TimelineDot>
				<TimelineConnector />
			  </TimelineSeparator>
			  <TimelineContent sx={{ py: '32px', px: 2 }}>
				<Typography variant="h6" component="span">
					Warden Closet X Armity (2<sup>nd</sup> collaboration)
				</Typography>
				<Typography>Engage with the Armity app</Typography>
			  </TimelineContent>
			</TimelineItem>

			<TimelineItem>
			  <TimelineOppositeContent
				sx={{ m: 'auto 0' }}
				align="right"
				variant="body2"
				color="text.secondary"
			  >
				5 March 2022
			  </TimelineOppositeContent>
			  <TimelineSeparator>
				<TimelineConnector />
				<TimelineDot color="primary">
				  <AutoAwesomeIcon />
				</TimelineDot>
				<TimelineConnector />
			  </TimelineSeparator>
			  <TimelineContent sx={{ py: '32px', px: 2 }}>
				<Typography variant="h6" component="span">
					Initiate NFT
				</Typography>
				<Typography>MetaWarden Closet (MWC) is NFT based on digital twin concept</Typography>
			  </TimelineContent>
			</TimelineItem>

			<TimelineItem>
			  <TimelineOppositeContent
				sx={{ m: 'auto 0' }}
				variant="body2"
				color="text.secondary"
			  >
				Q2 2022
			  </TimelineOppositeContent>
			  <TimelineSeparator>
				<TimelineConnector />
				<TimelineDot>
				  <LaptopMacIcon />
				</TimelineDot>
				<TimelineConnector />
			  </TimelineSeparator>
			  <TimelineContent sx={{ py: '32px', px: 2 }}>
				<Typography variant="h6" component="span">
				  Mint
				</Typography>
				<Typography>TBA</Typography>
			  </TimelineContent>
			</TimelineItem>

			<TimelineItem>
				<TimelineOppositeContent
				sx={{ m: 'auto 0' }}
				variant="body2"
				color="text.secondary"
			  >
				Q3 2022
			  </TimelineOppositeContent>
			  <TimelineSeparator>
				<TimelineConnector />
				<TimelineDot >
				  <RedeemIcon />
				</TimelineDot>
				<TimelineConnector />
			  </TimelineSeparator>
			  <TimelineContent sx={{ py: '32px', px: 2 }}>
				<Typography variant="h6" component="span">
				  Claim
				</Typography>
				<Typography>Claim your NFT item in real world</Typography>
			  </TimelineContent>
			</TimelineItem>

			<TimelineItem>
				<TimelineOppositeContent
				sx={{ m: 'auto 0' }}
				variant="body2"
				color="text.secondary"
			  >
				Q4 2022
			  </TimelineOppositeContent>
			  <TimelineSeparator>
				<TimelineConnector />
				<TimelineDot >
				  <PhonelinkIcon />
				</TimelineDot>
				<TimelineConnector />
			  </TimelineSeparator>
			  <TimelineContent sx={{ py: '32px', px: 2 }}>
				<Typography variant="h6" component="span">
					Privilege
				</Typography>
				<Typography>Access privilege function</Typography>
			  </TimelineContent>
			</TimelineItem>

			<TimelineItem>
				<TimelineOppositeContent
				sx={{ m: 'auto 0' }}
				variant="body2"
				color="text.secondary"
			  >
				Q1 2023
			  </TimelineOppositeContent>
			  <TimelineSeparator>
				<TimelineConnector />
				<TimelineDot >
				  <PeopleIcon />
				</TimelineDot>
				<TimelineConnector />
			  </TimelineSeparator>
			  <TimelineContent sx={{ py: '32px', px: 2 }}>
				<Typography variant="h6" component="span">
					MWC X Armity
				</Typography>
				<Typography>Use MWC in Armity</Typography>
			  </TimelineContent>
			</TimelineItem>
			
			<TimelineItem>
			<TimelineOppositeContent
				sx={{ m: 'auto 0' }}
				variant="body2"
				color="text.secondary"
			  >
			  </TimelineOppositeContent>
			  <TimelineSeparator>
				<TimelineConnector  />
				<TimelineDot color="secondary">
				  <MoreHorizIcon />
				</TimelineDot>
				<TimelineConnector />
			  </TimelineSeparator>
			  <TimelineContent sx={{ py: '32px', px: 2 }}>
				<Typography variant="h6" component="span">
					To be continued
				</Typography>
				<Typography></Typography>
			  </TimelineContent>
			</TimelineItem>
		  </Timeline>
		);
	  }

    render(){
        return (
            <Grid container justifyContent="center" alignItems="center" sx={{ height: '95vh' }}   >
				{/* <Grid container  md={12} lg={12} xl={12}>
					<marquee className="Mint-Top-Marquee">This content are not yet official &middot; This content are not yet official &middot; This content are not yet official</marquee>
				</Grid> */}

				<Grid container  md={12} lg={12} xl={12}>
					<img className='Mint-logo' src={require('../assets/img/logo-closet-whitebg.png')} alt="logo" />
				</Grid>

				<Grid container  md={12} lg={12} xl={8} className="Mint-Showcase-container">
					<Grid item sm={12} md={6} lg={3} xl={3}>
						<img className='Mint-Showcase-Item' src={require('../assets/img/logo-closet-galaxybg.png')} alt="logo" />
					</Grid>
					<Grid item sm={12} md={6} lg={3} xl={3}>
						<img className='Mint-Showcase-Item' src={require('../assets/img/logo-closet-galaxybg.png')} alt="logo" />
					</Grid>
					<Grid item sm={12} md={6} lg={3} xl={3}>
						<img className='Mint-Showcase-Item' src={require('../assets/img/logo-closet-galaxybg.png')} alt="logo" />
					</Grid>
					<Grid item sm={12} md={6} lg={3} xl={3}>
						<img className='Mint-Showcase-Item' src={require('../assets/img/logo-closet-galaxybg.png')} alt="logo" />
					</Grid>
				</Grid>

				<Grid container  md={12} lg={12} xl={12}>
					<Grid item xs={12} sm={12} md={12} lg={12} xl={12} style={{textAlign:'center' }} >
						<div className='Mint-NumberOfMint'>
							{this.state.numberOfMinted}/x000 Minted
						</div>
						<Button variant="contained" component="span" color="secondary" size="large" onClick={this.onClickConnectWalletBtn}>
							Connect Wallet
						</Button>

					</Grid>


					<Grid item xs={12} sm={12} md={12} lg={12} xl={12} className="Mint-Timeline-Container" >
						<div className='Mint-Title-Timeline'>Timeline</div>
						{this.BasicTimeline()}
					</Grid>


				</Grid>
            </Grid>
        )
    }
}

function mapStateToProps(state){
    return {
        // uploadBody: state.uploadBody
    }
}

function mapDispatchToProps(dispatch){
  return {
    // UPLOADBODY_RESET: () => {
    //   dispatch({type: 'UPLOADBODY_RESET', payload: null})
    // },
    actions: bindActionCreators(actions, dispatch),
  }
}

export default connect(mapStateToProps,mapDispatchToProps) (Mint) 