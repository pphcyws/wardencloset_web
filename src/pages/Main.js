import React, { Component } from 'react'
import * as actions from '../actions'
import { bindActionCreators } from 'redux'; 
import {connect} from 'react-redux'
import Button from '@mui/material/Button';
import { styled } from '@mui/material/styles';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';
import { TwitterIcon, TwitterShareButton } from "react-share";
import mwData from '../assets/json/mwData.json';
import './Main.css'
import { API_HOST } from '../constants';

let lastBodyUpload = null;
let lastHeadUpload = null;
let lastBGUpload = null;
let lastAcessoriesUpload = null;
let lastMaskUpload = null;
let lastLeftUpload = null;
let lastRightUpload = null;
let lastBottomUpload = null;
let lastShoeUpload = null;


const uploadXY = 1024;

const Input = styled('input')({ display: 'none' });

let metaWardenIDList = []
for (let i=11; i<=2999; i++){ metaWardenIDList.push(i.toString()); }

let url = `${API_HOST}/v2?`
const IMAGE_HOST = `${API_HOST}`

const metaWardenBodyList = [
    { label: 'Warden White Hood', value: 'body_1' },
    { label: 'KULAP Hood', value: 'body_2' },
    { label: 'Warden Galaxy Hood', value: 'body_3' },
    { label: 'Crypto Dog Hood', value: 'body_4' },
    { label: 'Black Suit', value: 'body_5' },
    { label: 'Pink Suit', value: 'body_6' },
    { label: 'Mcdonald', value: 'body_7' },
    { label: 'Knight', value: 'body_9' },
    { label: 'Chinese Qipao', value: 'body_10'},
    { label: 'IP Man', value: 'body_11'},
    { label: 'Kim Property Live (Insider)', value: 'body_12' },
    { label: 'Kim Property Live (Onlyfans)', value: 'body_19' },
    { label: 'Sobta Cafe&Co', value: 'body_13' },
    { label: 'Kimdano', value: 'body_14' },
    { label: 'Lu Bu', value: 'body_15' },
    { label: 'Bitcoin', value: 'body_16' },
    { label: 'Miyagi', value: 'body_17'},
    { label: 'RAP IS NOW T-Shirt', value: 'body_24' },
    { label: 'RAP IS NOW Hood', value: 'body_25' },
    { label: 'Your Brand', value: 'body_18'},
    { label: 'Still Single Pink', value: 'body_20' },
    { label: 'Still Single White', value: 'body_21' },
    { label: 'Valentine', value: 'body_22' },
    { label: 'Squid Game', value: 'body_26' },
    { label: 'Tanjiro', value: 'body_27' },
    { label: 'Zenitsu', value: 'body_28' },
    { label: 'Chill Decorate', value: 'body_29' },
    { label: 'อากาศธาตุ', value: 'body_30' },
    { label: 'Kim Property Live (Staff Uniform)', value: 'body_31' },
    { label: 'Rengoku', value: 'body_32' },
    { label: 'Giyu', value: 'body_33' },
    { label: 'Wedding', value: 'body_35' },
    { label: 'Stocker Day', value: 'body_36' },
    { label: 'Doctor', value: 'body_37' },
    { label: 'Chiangmai Crypto City 1', value: 'body_38' },
    { label: 'Chiangmai Crypto City 2', value: 'body_39' },
    { label: 'ATEVADA', value: 'body_40' },
    { label: 'Attack on Titan', value: 'body_41' },
    { label: 'Block Mountain', value: 'body_42' },
    { label: 'Contribution Dao', value: 'body_43' },
    { label: 'Kasou-Shin', value: 'body_44' },
    { label: 'Songkran', value: 'body_45' },
    { label: 'Upload Your Clothing', value: 'upload' },

]

const metaWardenModeList = [
    { label: 'Half', value: 'half' },
    { label: 'Full', value: 'full' },
    { label: 'Back', value: 'back' }     
]

const metaWardenStyleList = [
    { label: 'None', value: 'none' },
    { label: 'Mosaic', value: 'mosaic' },
]

const metaWardenBGList = [
    { label: 'Love', value: 'bg_1' },
    { label: 'Upload Your Background', value: 'upload'}
];

const metaWardenHeadList = [
    { label: 'Standard', value: 'head_1' },
    { label: 'Mcdonald Cap', value: 'head_2' },
    { label: 'Knight Helmet', value: 'head_3' },
    { label: 'Lu Bu', value: 'head_4' },
    { label: 'Miyagi', value: 'head_5' },
    { label: 'RAP IS NOW Cap', value: 'head_6' },
    { label: 'Chill Decorate', value: 'head_7' },
    { label: 'Enosuke', value: 'head_8' },
    { label: 'Girl', value: 'head_9' },
    { label: 'Stocker Day', value: 'head_10' },
    { label: 'Chiangmai Crypto City', value: 'head_11' },
    { label: 'Attack on Titan', value: 'head_12' },
    { label: 'Contribution Dao', value: 'head_13' },
    { label: 'Kasou-Shin', value: 'head_14' },
    { label: 'TDKill', value: 'head_15' },
    { label: 'Upload Your Headware', value: 'upload' },
];

const metaWardenAccessoriesList = [
    { label: 'Stairs', value: 'accessories_1' },
    { label: 'Nezuko Box', value: 'accessories_2' },
    { label: 'Songkran Gun', value: 'accessories_3' },
    { label: 'Upload Your Accessories', value: 'upload' },
];

let queryDict = { mode: 'half'};
class Main extends Component {
    state = {
        metaWardenIDList : metaWardenIDList,
        metaWardenIDValue: "",

        metaWardenModeList : metaWardenModeList,
        metaWardenModeValue: { label: 'Half', value: 'half' },

        metaWardenBGList : metaWardenBGList,
        metaWardenBGValue: null,

        metaWardenHeadList: metaWardenHeadList,
        metaWardenHeadValue: null,

        metaWardenMaskList: [
            { label: 'None', value: null },
            { label: 'ATEVADA - VANARA', value: 'mask_1' },
            { label: 'ATEVADA - YAKSA', value: 'mask_2' },
            { label: 'ATEVADA - NAGA', value: 'mask_3' },
            { label: 'ATEVADA - GARUDA', value: 'mask_4' },
            { label: 'ATEVADA - VETAL', value: 'mask_5' },
            { label: 'ATEVADA - DEVA', value: 'mask_6' },
            { label: 'Upload Your Mask', value: 'upload' },
        ],

        metaWardenBodyList: metaWardenBodyList,
        metaWardenBodyValue: null,

        metaWardenAccessoriesList: metaWardenAccessoriesList,
        metaWardenAccessoriesValue: null,

        metaWardenStyleList: metaWardenStyleList,
        metaWardenStyleValue: { label: 'None', value: null },

        
        
        metaWardenLeftList: [
            { label: 'None', value: null },
            { label: 'Flower', value: 'left_1' },
            { label: 'TDKill', value: 'left_2' },
            { label: 'Upload Left Hand', value: 'upload' },
            
        ],
        metaWardenLeftValue: { label: 'None', value: null },

        metaWardenRightList: [
            { label: 'None', value: null },
            { label: 'BTC Watch', value: 'right_1' },
            { label: 'ETH Watch', value: 'right_2' },
            { label: 'WAD Watch', value: 'right_3' },
            { label: 'ADA Watch', value: 'right_4' },
            { label: 'Upload Right Hand', value: 'upload' },
        ],
        metaWardenRightValue: { label: 'None', value: null },

        metaWardenTrousersList: [
            { label: 'None', value: null },
            { label: 'Black Trousers', value: 'bottom_1' },
            { label: 'White Trousers', value: 'bottom_20' },
            { label: 'Gray Trousers', value: 'bottom_21' },
            { label: 'Light Blue Trousers', value: 'bottom_22' },
            { label: 'Pink Trousers', value: 'bottom_23' },
            { label: 'Green Trousers', value: 'bottom_24' },
            { label: 'Khaki Trousers', value: 'bottom_25' },
            { label: 'White Shorts', value: 'bottom_2' },
            { label: 'Light Pink Shorts', value: 'bottom_3' },
            { label: 'Light Green Shorts', value: 'bottom_4' },
            { label: 'Pink Shorts', value: 'bottom_5' },
            { label: 'Gray Shorts', value: 'bottom_6' },
            { label: 'Light Blue Shorts', value: 'bottom_7' },
            { label: 'Light Khaki Shorts', value: 'bottom_8' },
            { label: 'Khaki Shorts', value: 'bottom_9' },
            { label: 'Gray Skirt', value: 'bottom_10' },
            { label: 'Pink Skirt', value: 'bottom_11' },
            { label: 'Red Skirt', value: 'bottom_12' },
            { label: 'White Skirt', value: 'bottom_13' },
            { label: 'Crimson Skirt', value: 'bottom_14' },
            { label: 'Purple Skirt', value: 'bottom_15' },
            { label: 'Light Gray Skirt', value: 'bottom_16' },
            { label: 'Dark Green Skirt', value: 'bottom_17' },
            { label: 'Dark Blue Skirt', value: 'bottom_18' },
            { label: 'Khaki Skirt', value: 'bottom_19' },
            { label: 'Original Black With Belt', value: 'bottom_26' },
            { label: 'Original Red With Belt', value: 'bottom_27' },
            { label: 'Original Red', value: 'bottom_28' },
            { label: 'Original Blue With Belt', value: 'bottom_29' },
            { label: 'Original Blue', value: 'bottom_30' },
            { label: 'Original Pink', value: 'bottom_31' },
            { label: 'Aristocrat Green', value: 'bottom_32' },
            { label: 'Aristocrat Apricot', value: 'bottom_33' },
            { label: 'Attack on Titan', value: 'bottom_34' },
            { label: 'Block Mountain', value: 'bottom_35' },
            { label: 'Kasou-Shin', value: 'bottom_36' },
            { label: 'Upload Your Trouser/Skirt', value: 'upload' },
        ],
        metaWardenTrousersValue: { label: 'None', value: null },


        metaWardenShoeList: [
            { label: 'None', value: null },
            { label: 'Black Shoe', value: 'shoe_1' },
            { label: 'Black Sneaker', value: 'shoe_2' },
            { label: 'Red Sneaker', value: 'shoe_3' },
            { label: 'Blue Sneaker', value: 'shoe_4' },
            { label: 'Green Sneaker', value: 'shoe_5' },
            { label: 'Orange Sneaker', value: 'shoe_6' },
            { label: 'Pink Sneaker', value: 'shoe_7' },
            { label: 'Light Blue Sneaker', value: 'shoe_8' },
            { label: 'White Sneaker', value: 'shoe_9' },
            { label: 'Rengoku Boots', value: 'shoe_10' },
            { label: 'Giyu Boots', value: 'shoe_11' },
            { label: 'Attack on Titan', value: 'shoe_12' },
            { label: 'Upload Your Shoe', value: 'upload' }
        ],

        metaWardenShoeValue: { label: 'None', value: null },

        metaWardenBagList: [
            { label: 'None', value: null },
            { label: 'Pink Bag', value: 'back_1' },
            { label: 'Gray Bag', value: 'back_2' },
            { label: 'Light Blue Bag', value: 'back_3' },
            { label: 'Light Green Bag', value: 'back_4' },
        ],
        metaWardenBagValue: { label: 'None', value: null },

        metaWardenCarList: [
            { label: 'None', value: null },
            { label: 'Wen Lambo', value: 'car_2' },
        ],
        metaWardenCarValue: { label: 'None', value: null },

        src: `${url}id=3000`,
        sharedId: 'default',

        isBodyUpload: false,
        isHeadUpload: false,
        isBGUpload: false,
        isSnackbarOpen: false,
        isLoadingImage: false,

        warningText: "Choose MetaWarden ID first!"
    }

    UNSAFE_componentWillReceiveProps(nextProps){
        let uploadBody = nextProps.uploadBody
        // console.log(uploadBody.data)

        if(uploadBody.data && !uploadBody.isFailed && !uploadBody.isLoading) {
            let base64 =uploadBody.data.base64;
            this.setState({ src : base64 });

            this.props.UPLOADBODY_RESET();
        }

    }

    metaWardenIDChange = (e,v,r) => { 
        this.setState({isLoadingImage: true})
        queryDict["id"] = v;
        const MWADMD = this.getMwMetadata(v);
        queryDict["base"] = MWADMD["Class"];
        queryDict["background"] = MWADMD["Background"];
        queryDict["body"] = MWADMD["Body"];
        if (MWADMD["Accessories"] !== 'none'){
            queryDict["accessories"] = MWADMD["Accessories"].trim()
        }

        //set defalut "bottom"
        let _metaWardenMaskValue = { label: 'None', value: null };
        if (MWADMD["Body"] === "Aristocrat Apricot"){ queryDict["bottom"] = "bottom_33"; _metaWardenMaskValue = { label: 'Aristocrat Apricot', value: 'bottom_33' }; }
        if (MWADMD["Body"] === "Aristocrat Green"){ queryDict["bottom"] = "bottom_32"; _metaWardenMaskValue = { label: 'Aristocrat Apricot', value: 'bottom_33' }; }
        if (MWADMD["Body"] === "Basketball Jersey Sannoh"){ queryDict["bottom"] = "bottom_28"; _metaWardenMaskValue = { label: 'Original Red', value: 'bottom_28' }; }
        if (MWADMD["Body"] === "Basketball Jersey Shohoku"){ queryDict["bottom"] = "bottom_28"; _metaWardenMaskValue = { label: 'Original Red', value: 'bottom_28' }; }
        if (MWADMD["Body"] === "Cape Anpan Man"){ queryDict["bottom"] = "bottom_28"; _metaWardenMaskValue = { label: 'Original Red', value: 'bottom_28' }; }
        if (MWADMD["Body"] === "Cape Lucky Man"){ queryDict["bottom"] = "bottom_28"; _metaWardenMaskValue = { label: 'Original Red', value: 'bottom_28' }; }
        if (MWADMD["Body"] === "Cape One Punch Man"){ queryDict["bottom"] = "bottom_28"; _metaWardenMaskValue = { label: 'Original Red', value: 'bottom_28' }; }
        if (MWADMD["Body"] === "Cape Shining Golden"){ queryDict["bottom"] = "bottom_9";  _metaWardenMaskValue = { label: 'Khaki Shorts', value: 'bottom_9' }; }
        if (MWADMD["Body"] === "Cape Shining Midnight"){ queryDict["bottom"] = "bottom_9"; _metaWardenMaskValue = { label: 'Khaki Shorts', value: 'bottom_9' }; }
        if (MWADMD["Body"] === "Cape Super Man"){ queryDict["bottom"] = "bottom_30"; _metaWardenMaskValue = { label: 'Original Blue', value: 'bottom_30' }; }
        if (MWADMD["Body"] === "Double Breasted"){ queryDict["bottom"] = "bottom_30"; _metaWardenMaskValue = { label: 'Original Blue', value: 'bottom_30' };  }
        if (MWADMD["Body"] === "Fantasy Scraft"){ queryDict["bottom"] = "bottom_30"; _metaWardenMaskValue = { label: 'Original Blue', value: 'bottom_30' }; }
        if (MWADMD["Body"] === "Groom Suit"){ queryDict["bottom"] = "bottom_20"; _metaWardenMaskValue = { label: 'White Trousers', value: 'bottom_20' };  }
        if (MWADMD["Body"] === "Groomsmen Suspenders Grey"){ queryDict["bottom"] = "bottom_21"; _metaWardenMaskValue = { label: 'Gray Trousers', value: 'bottom_21' };  }
        if (MWADMD["Body"] === "Groomsmen Suspenders Orange"){ queryDict["bottom"] = "bottom_21"; _metaWardenMaskValue = { label: 'Aristocrat Apricot', value: 'bottom_33' };  }
        if (MWADMD["Body"] === "Hoodie Black"){ queryDict["bottom"] = "bottom_21"; _metaWardenMaskValue = { label: 'Gray Trousers', value: 'bottom_21' };  }
        if (MWADMD["Body"] === "Hoodie Blue"){ queryDict["bottom"] = "bottom_22"; _metaWardenMaskValue = { label: 'Light Blue Trousers', value: 'bottom_22' };  }
        if (MWADMD["Body"] === "Hoodie Pink"){ queryDict["bottom"] = "bottom_23"; _metaWardenMaskValue = { label: 'Pink Trousers', value: 'bottom_23' };  }
        if (MWADMD["Body"] === "Hoodie Red"){ queryDict["bottom"] = "bottom_23"; _metaWardenMaskValue = { label: 'Original Red', value: 'bottom_28' };  }
        if (MWADMD["Body"] === "Hoodie Space"){ queryDict["bottom"] = "bottom_1"; _metaWardenMaskValue = { label: 'Black Trousers', value: 'bottom_1' };  }
        if (MWADMD["Body"] === "Ninja Amegakure"){ queryDict["bottom"] = "bottom_1"; _metaWardenMaskValue = { label: 'Black Trousers', value: 'bottom_1' };  }
        if (MWADMD["Body"] === "Ninja Hoshigakure"){ queryDict["bottom"] = "bottom_1"; _metaWardenMaskValue = { label: 'Black Trousers', value: 'bottom_1' };  }
        if (MWADMD["Body"] === "Ninja Ishigakure"){ queryDict["bottom"] = "bottom_1"; _metaWardenMaskValue = { label: 'Black Trousers', value: 'bottom_1' };  }
        if (MWADMD["Body"] === "Ninja Konohagakure"){ queryDict["bottom"] = "bottom_1"; _metaWardenMaskValue = { label: 'Black Trousers', value: 'bottom_1' };  }
        if (MWADMD["Body"] === "Single Breasted"){ queryDict["bottom"] = "bottom_21"; _metaWardenMaskValue = { label: 'Gray Trousers', value: 'bottom_21' };  }
        if (MWADMD["Body"] === "Spacesuite Flight"){ queryDict["bottom"] = "bottom_20"; _metaWardenMaskValue = { label: 'White Trousers', value: 'bottom_20' };  }
        if (MWADMD["Body"] === "Spacesuite Marauder"){ queryDict["bottom"] = "bottom_21"; _metaWardenMaskValue = { label: 'Gray Trousers', value: 'bottom_21' };  }
        if (MWADMD["Body"] === "Spacesuite Sharpshooter"){ queryDict["bottom"] = "bottom_20"; _metaWardenMaskValue = { label: 'White Trousers', value: 'bottom_20' }; }
        if (MWADMD["Body"] === "T-Shirt Arm-Striped Blue"){ queryDict["bottom"] = "bottom_22"; _metaWardenMaskValue = { label: 'Light Blue Trousers', value: 'bottom_22' }; }
        if (MWADMD["Body"] === "T-Shirt Plain-Blue"){ queryDict["bottom"] = "bottom_22"; _metaWardenMaskValue = { label: 'Light Blue Trousers', value: 'bottom_22' }; }
        if (MWADMD["Body"] === "T-Shirt Striped-Blue"){ queryDict["bottom"] = "bottom_22"; _metaWardenMaskValue = { label: 'Light Blue Trousers', value: 'bottom_22' }; }

        if (MWADMD["Body"] === "T-Shirt Faded-Blue"){ queryDict["bottom"] = "bottom_20"; _metaWardenMaskValue = { label: 'White Trousers', value: 'bottom_20' }; }
        if (MWADMD["Body"] === "T-Shirt Faded-Green"){ queryDict["bottom"] = "bottom_20"; _metaWardenMaskValue = { label: 'White Trousers', value: 'bottom_20' }; }
        if (MWADMD["Body"] === "T-Shirt Plain White"){ queryDict["bottom"] = "bottom_20"; _metaWardenMaskValue = { label: 'White Trousers', value: 'bottom_20' }; }
        if (MWADMD["Body"] === "T-Shirt Tie-Dye Purple"){ queryDict["bottom"] = "bottom_20"; _metaWardenMaskValue = { label: 'White Trousers', value: 'bottom_20' }; }
        if (MWADMD["Body"] === "T-Shirt Plain Orange"){ queryDict["bottom"] = "bottom_20"; _metaWardenMaskValue = { label: 'White Trousers', value: 'bottom_20' }; }

        if (MWADMD["Body"] === "T-Shirt Plain-Red"){ queryDict["bottom"] = "bottom_27"; _metaWardenMaskValue = { label: 'Original Red With Belt', value: 'bottom_27' }; }
        if (MWADMD["Body"] === "T-Shirt Striped-Red"){ queryDict["bottom"] = "bottom_27"; _metaWardenMaskValue = { label: 'Original Red With Belt', value: 'bottom_27' }; }
        if (MWADMD["Body"] === "T-Shirt Striped-Red"){ queryDict["bottom"] = "bottom_27"; _metaWardenMaskValue = { label: 'Original Red With Belt', value: 'bottom_27' }; }

        queryDict["face"] = MWADMD["Face"];
        queryDict["head"] = MWADMD["Head"];

        let src = `${url}${Object.keys(queryDict).map(key => key + '=' + queryDict[key]).join('&')}`;

        //Background
        let BGList = [...metaWardenBGList];
        BGList.push({ label: `${MWADMD["Background"]} (Original)`, value: MWADMD["Background"] } )
        
        //Body
        let BodyList = [...metaWardenBodyList];
        BodyList.push({ label: `${MWADMD["Body"]} (Original)`, value: MWADMD["Body"] } )

        //Head
        let HeadList = [...metaWardenHeadList];
        HeadList.push({ label: `${MWADMD["Head"]} (Original)`, value: MWADMD["Head"] } )
        
        //Accessories
        let AccessoriesList = [...metaWardenAccessoriesList];
        AccessoriesList.push({ label: `${MWADMD["Accessories"]} (Original)`, value: MWADMD["Accessories"] } )


        this.setState({ 
            metaWardenIDValue: v,
            metaWardenBGList: BGList,
            metaWardenBGValue: BGList[BGList.length-1],

            metaWardenBodyList: BodyList,
            metaWardenBodyValue: BodyList[BodyList.length-1],

            metaWardenHeadList: HeadList,
            metaWardenHeadValue: HeadList[HeadList.length-1],

            // metaWardenAccessoriesList: AccessoriesList,
            metaWardenAccessoriesValue: AccessoriesList[AccessoriesList.length-1],

            metaWardenMaskValue: _metaWardenMaskValue,

            isBodyUpload: false,
            isHeadUpload: false,
            isAccessoriesUpload: false,
            isMaskUpload: false,
            src: src 
        })

    }

    getMwMetadata = (id) => {
        for (let i in mwData) { 
            if (parseInt(mwData[i]["ID"], 10) === parseInt(id, 10)) { 
                return mwData[i]; 
            } 
        }
        return null;
    }

    getBody = (baseValue, id) => {
        let mwIDData = this.getMwMetadata(id);
        // console.log(mwIDData)
        let Class = mwIDData.Class
        let Body = mwIDData.Body
        if (Body === baseValue){ baseValue = `${baseValue}`; }
        else if (Class === "Golden"){ baseValue = `common_${baseValue}`; }
        else{ baseValue = `${Class.toLowerCase()}_${baseValue}`; }
        
        return baseValue;
    }

    metaWardenHeadChange = (e,v,r) => { 
        if (!this.state.metaWardenIDValue) {
            this.setState({isSnackbarOpen: true, warningText: "Choose MetaWarden ID first!" });
            return;
        }

        this.setState({isLoadingImage: true})

        if (v.value !== 'upload') { 
            queryDict["head"] = v.value;
            if ( (this.state.isBodyUpload === true && lastBodyUpload) || (this.state.isHeadUpload === true && lastHeadUpload) || (this.state.isBGUpload === true && lastBGUpload) || (this.state.isAccessoriesUpload === true && lastAcessoriesUpload)) {
                //Some component is "upload" selected
                let data = new FormData();
                if (lastBodyUpload) { data.append('upload-body', lastBodyUpload ); }
                if (lastBGUpload) { data.append('upload-background', lastBGUpload ); }
                if (lastAcessoriesUpload) { data.append('upload-accessories', lastAcessoriesUpload ); }
                if (lastMaskUpload) { data.append('upload-mask', lastMaskUpload ); }
                if (lastLeftUpload) { data.append('upload-left', lastLeftUpload ); }
                if (lastRightUpload) { data.append('upload-right', lastRightUpload ); }
                if (lastShoeUpload) { data.append('upload-shoe', lastShoeUpload ); }
                if (lastBottomUpload) { data.append('upload-bottom', lastBottomUpload ); }
    
                data.append('data', JSON.stringify(queryDict))
                this.props.actions.uploadBody(data);

                this.setState({ metaWardenHeadValue: v, isHeadUpload: false });

            }
            else{
                let src = `${url}${Object.keys(queryDict).map(key => key + '=' + queryDict[key]).join('&')}`;
                this.setState({ metaWardenHeadValue: v, src: src, isHeadUpload: false });
            }
        }
        else if (v.value === 'upload'){
            this.setState({ metaWardenHeadValue: v, isHeadUpload: true })
        }
    }

    
    metaWardenTrousersChange = (e,v,r) => { 
        if (!this.state.metaWardenIDValue) {
            this.setState({isSnackbarOpen: true, warningText: "Choose MetaWarden ID first!" });
            return;
        }

        this.setState({isLoadingImage: true})

        if (v.value !== 'upload') { 
            queryDict["bottom"] = v.value;
            if ( (this.state.isBodyUpload === true && lastBodyUpload) || (this.state.isHeadUpload === true && lastHeadUpload) || (this.state.isBGUpload === true && lastBGUpload) || (this.state.isAccessoriesUpload === true && lastAcessoriesUpload)) {
                //Some component is "upload" selected
                let data = new FormData();
                if (lastBodyUpload) { data.append('upload-body', lastBodyUpload ); }
                if (lastHeadUpload) { data.append('upload-head', lastHeadUpload ); }
                if (lastBGUpload) { data.append('upload-background', lastBGUpload ); }
                if (lastAcessoriesUpload) { data.append('upload-accessories', lastAcessoriesUpload ); }
                if (lastMaskUpload) { data.append('upload-mask', lastMaskUpload ); }
                if (lastLeftUpload) { data.append('upload-left', lastLeftUpload ); }
                if (lastRightUpload) { data.append('upload-right', lastRightUpload ); }
                if (lastShoeUpload) { data.append('upload-shoe', lastShoeUpload ); }
    
                data.append('data', JSON.stringify(queryDict))
                this.props.actions.uploadBody(data);

                this.setState({ metaWardenTrousersValue: v, isBottomUpload: false });

            }
            else{
                let src = `${url}${Object.keys(queryDict).map(key => key + '=' + queryDict[key]).join('&')}`;
                this.setState({ metaWardenTrousersValue: v, src: src, isBottomUpload: false });
            }
        }
        else if (v.value === 'upload'){
            this.setState({ metaWardenTrousersValue: v, isBottomUpload: true })
        }
        
    }

    metaWardenModeChange = (e,v,r) => { 
        this.setState({isLoadingImage: true})

        if (v.value !== 'mode') { 
            queryDict["mode"] = v.value;
            if (v.value === null) { delete queryDict["mode"]; }
            if ( (this.state.isBodyUpload === true && lastBodyUpload) || (this.state.isHeadUpload === true && lastHeadUpload) || (this.state.isBGUpload === true && lastBGUpload) || (this.state.isAccessoriesUpload === true && lastAcessoriesUpload)) {
                //Some component is "upload" selected
                let data = new FormData();
                if (lastBodyUpload) { data.append('upload-body', lastBodyUpload ); }
                if (lastHeadUpload) { data.append('upload-head', lastHeadUpload ); }
                if (lastBGUpload) { data.append('upload-background', lastBGUpload ); }
                if (lastAcessoriesUpload) { data.append('upload-accessories', lastAcessoriesUpload ); }
                if (lastMaskUpload) { data.append('upload-mask', lastMaskUpload ); }
                if (lastLeftUpload) { data.append('upload-left', lastLeftUpload ); }
                if (lastRightUpload) { data.append('upload-right', lastRightUpload ); }
                if (lastShoeUpload) { data.append('upload-shoe', lastShoeUpload ); }
                if (lastBottomUpload) { data.append('upload-bottom', lastBottomUpload ); }
    
                data.append('data', JSON.stringify(queryDict))
                this.props.actions.uploadBody(data);

                this.setState({ metaWardenModeValue: v });
            }
            else{
                let src = `${url}${Object.keys(queryDict).map(key => key + '=' + queryDict[key]).join('&')}`;
                this.setState({ metaWardenModeValue: v, src: src});
            }
        }
        else if (v.value === 'upload'){
            this.setState({ metaWardenModeValue: v })
        }

    }

    metaWardenBGChange = (e,v,r) => { 
        if (!this.state.metaWardenIDValue) {
            this.setState({isSnackbarOpen: true, warningText: "Choose MetaWarden ID first!" });
            return;
        }

        this.setState({isLoadingImage: true})

        if (v.value !== 'upload') { 
            queryDict["background"] = v.value;
            if ( (this.state.isBodyUpload === true && lastBodyUpload) || (this.state.isHeadUpload === true && lastHeadUpload) || (this.state.isBGUpload === true && lastBGUpload) || (this.state.isAccessoriesUpload === true && lastAcessoriesUpload)) {

                //Some component is "upload" selected
                let data = new FormData();
                if (lastBodyUpload) { data.append('upload-body', lastBodyUpload ); }
                if (lastHeadUpload) { data.append('upload-head', lastHeadUpload ); }
                if (lastAcessoriesUpload) { data.append('upload-accessories', lastAcessoriesUpload ); }
                if (lastMaskUpload) { data.append('upload-mask', lastMaskUpload ); }
                if (lastLeftUpload) { data.append('upload-left', lastLeftUpload ); }
                if (lastRightUpload) { data.append('upload-right', lastRightUpload ); }
                if (lastShoeUpload) { data.append('upload-shoe', lastShoeUpload ); }
                if (lastBottomUpload) { data.append('upload-bottom', lastBottomUpload ); }
    
                data.append('data', JSON.stringify(queryDict))
                this.props.actions.uploadBody(data);

                this.setState({ metaWardenBGValue: v, isBGUpload: false });
            }
            else{
                let src = `${url}${Object.keys(queryDict).map(key => key + '=' + queryDict[key]).join('&')}`;
                this.setState({ metaWardenBGValue: v, src: src, isBGUpload: false });
            }
        }
        else if (v.value === 'upload'){
            this.setState({ metaWardenBGValue: v, isBGUpload: true })
        }

    }

    metaWardenLeftChange = (e,v,r) => { 
        if (!this.state.metaWardenIDValue) {
            this.setState({isSnackbarOpen: true, warningText: "Choose MetaWarden ID first!" });
            return;
        }

        this.setState({isLoadingImage: true})

        if (v.value !== 'upload') { 
            queryDict["left"] = v.value;
            if (v.value === null) { delete queryDict["left"]; }
            if ( (this.state.isBodyUpload === true && lastBodyUpload) || (this.state.isHeadUpload === true && lastHeadUpload) || (this.state.isBGUpload === true && lastBGUpload) || (this.state.isAccessoriesUpload === true && lastAcessoriesUpload)) {

                //Some component is "upload" selected
                let data = new FormData();
                if (lastBodyUpload) { data.append('upload-body', lastBodyUpload ); }
                if (lastHeadUpload) { data.append('upload-head', lastHeadUpload ); }
                if (lastBGUpload) { data.append('upload-background', lastBGUpload ); }
                if (lastAcessoriesUpload) { data.append('upload-accessories', lastAcessoriesUpload ); }
                if (lastMaskUpload) { data.append('upload-mask', lastMaskUpload ); }
                if (lastRightUpload) { data.append('upload-right', lastRightUpload ); }
                if (lastShoeUpload) { data.append('upload-shoe', lastShoeUpload ); }
                if (lastBottomUpload) { data.append('upload-bottom', lastBottomUpload ); }
    
                data.append('data', JSON.stringify(queryDict))
                this.props.actions.uploadBody(data);

                this.setState({ metaWardenLeftValue: v, isLeftUpload: false });
            }
            else{
                let src = `${url}${Object.keys(queryDict).map(key => key + '=' + queryDict[key]).join('&')}`;
                this.setState({ metaWardenLeftValue: v, src: src, isLeftUpload: false});
            }
        }
        else if (v.value === 'upload'){
            this.setState({ metaWardenLeftValue: v, isLeftUpload: true })
        }

    }

    metaWardenRightChange = (e,v,r) => { 
        if (!this.state.metaWardenIDValue) {
            this.setState({isSnackbarOpen: true, warningText: "Choose MetaWarden ID first!" });
            return;
        }

        this.setState({isLoadingImage: true})

        if (v.value !== 'upload') { 
            queryDict["right"] = v.value;
            if (v.value === null) { delete queryDict["right"]; }
            if ( (this.state.isBodyUpload === true && lastBodyUpload) || (this.state.isHeadUpload === true && lastHeadUpload) || (this.state.isBGUpload === true && lastBGUpload) || (this.state.isAccessoriesUpload === true && lastAcessoriesUpload)) {

                //Some component is "upload" selected
                let data = new FormData();
                if (lastBodyUpload) { data.append('upload-body', lastBodyUpload ); }
                if (lastHeadUpload) { data.append('upload-head', lastHeadUpload ); }
                if (lastBGUpload) { data.append('upload-background', lastBGUpload ); }
                if (lastAcessoriesUpload) { data.append('upload-accessories', lastAcessoriesUpload ); }
                if (lastMaskUpload) { data.append('upload-mask', lastMaskUpload ); }
                if (lastLeftUpload) { data.append('upload-left', lastLeftUpload ); }
                if (lastShoeUpload) { data.append('upload-shoe', lastShoeUpload ); }
                if (lastBottomUpload) { data.append('upload-bottom', lastBottomUpload ); }
    
                data.append('data', JSON.stringify(queryDict))
                this.props.actions.uploadBody(data);

                this.setState({ metaWardenRightValue: v, isRightUpload: false });
            }
            else{
                let src = `${url}${Object.keys(queryDict).map(key => key + '=' + queryDict[key]).join('&')}`;
                this.setState({ metaWardenRightValue: v, src: src, isRightUpload: false});
            }
        }
        else if (v.value === 'upload'){
            this.setState({ metaWardenRightValue: v, isRightUpload: true })
        }
    }

    metaWardenShoeChange = (e,v,r) => {
        if (!this.state.metaWardenIDValue) {
            this.setState({isSnackbarOpen: true, warningText: "Choose MetaWarden ID first!" });
            return;
        }

        this.setState({isLoadingImage: true})

        if (v.value !== 'upload') { 
            queryDict["shoe"] = v.value;
            if (v.value === null) { delete queryDict["shoe"]; }
            if ( (this.state.isBodyUpload === true && lastBodyUpload) || (this.state.isHeadUpload === true && lastHeadUpload) || (this.state.isBGUpload === true && lastBGUpload) || (this.state.isAccessoriesUpload === true && lastAcessoriesUpload)) {

                //Some component is "upload" selected
                let data = new FormData();
                if (lastBodyUpload) { data.append('upload-body', lastBodyUpload ); }
                if (lastHeadUpload) { data.append('upload-head', lastHeadUpload ); }
                if (lastBGUpload) { data.append('upload-background', lastBGUpload ); }
                if (lastAcessoriesUpload) { data.append('upload-accessories', lastAcessoriesUpload ); }
                if (lastMaskUpload) { data.append('upload-mask', lastMaskUpload ); }
                if (lastLeftUpload) { data.append('upload-left', lastLeftUpload ); }
                if (lastRightUpload) { data.append('upload-right', lastRightUpload ); }
                if (lastBottomUpload) { data.append('upload-bottom', lastBottomUpload ); }
    
                data.append('data', JSON.stringify(queryDict))
                this.props.actions.uploadBody(data);

                this.setState({ metaWardenShoeValue: v, isShoeUpload: false });
            }
            else{
                let src = `${url}${Object.keys(queryDict).map(key => key + '=' + queryDict[key]).join('&')}`;
                this.setState({ metaWardenShoeValue: v, src: src , isShoeUpload: false });
            }
        }
        else if (v.value === 'upload'){
            this.setState({ metaWardenShoeValue: v, isShoeUpload: true })
        }
    }

    metaWardenCarChange = (e,v,r) => { 
        if (!this.state.metaWardenIDValue) {
            this.setState({isSnackbarOpen: true, warningText: "Choose MetaWarden ID first!" });
            return;
        }

        this.setState({isLoadingImage: true})

        if (v.value !== 'car') { 
            queryDict["car"] = v.value;
            if (v.value === null) { delete queryDict["car"]; }
            if ( (this.state.isBodyUpload === true && lastBodyUpload) || (this.state.isHeadUpload === true && lastHeadUpload) || (this.state.isBGUpload === true && lastBGUpload) || (this.state.isAccessoriesUpload === true && lastAcessoriesUpload)) {

                //Some component is "upload" selected
                let data = new FormData();
                if (lastBodyUpload) { data.append('upload-body', lastBodyUpload ); }
                if (lastHeadUpload) { data.append('upload-head', lastHeadUpload ); }
                if (lastBGUpload) { data.append('upload-background', lastBGUpload ); }
                if (lastAcessoriesUpload) { data.append('upload-accessories', lastAcessoriesUpload ); }
                if (lastMaskUpload) { data.append('upload-mask', lastMaskUpload ); }
                if (lastLeftUpload) { data.append('upload-left', lastLeftUpload ); }
                if (lastRightUpload) { data.append('upload-right', lastRightUpload ); }
                if (lastShoeUpload) { data.append('upload-shoe', lastShoeUpload ); }
                if (lastBottomUpload) { data.append('upload-bottom', lastBottomUpload ); }
    
                data.append('data', JSON.stringify(queryDict))
                this.props.actions.uploadBody(data);

                this.setState({ metaWardenCarValue: v });
            }
            else{
                let src = `${url}${Object.keys(queryDict).map(key => key + '=' + queryDict[key]).join('&')}`;
                this.setState({ metaWardenCarValue: v, src: src});
            }
        }
        else if (v.value === 'upload'){
            this.setState({ metaWardenCarValue: v })
        }

    }

    metaWardenBagChange = (e,v,r) => {
        this.setState({isLoadingImage: true})

        queryDict["back"] = v.value;
        if (v.value === null) { delete queryDict["back"]; }

        let src = `${url}${Object.keys(queryDict).map(key => key + '=' + queryDict[key]).join('&')}`;

        this.setState({ metaWardenBagValueValue: v, src: src }); 
    }

    metaWardenBodyChange = (e,v,r) => { 
        //if don't have ID return false
        if (!this.state.metaWardenIDValue) {
            this.setState({isSnackbarOpen: true, warningText: "Choose MetaWarden ID first!" });
            return;
        }

        this.setState({isLoadingImage: true})

        if (v.value !== 'upload') { 
            queryDict["body"] = this.getBody(v.value, this.state.metaWardenIDValue);
            if ( (this.state.isBodyUpload === true && lastBodyUpload) || (this.state.isHeadUpload === true && lastHeadUpload) || (this.state.isBGUpload === true && lastBGUpload) || (this.state.isAccessoriesUpload === true && lastAcessoriesUpload)) {

                //Some component is "upload" selected
                let data = new FormData();
                if (lastHeadUpload) { data.append('upload-head', lastHeadUpload ); }
                if (lastBGUpload) { data.append('upload-background', lastBGUpload ); }
                if (lastAcessoriesUpload) { data.append('upload-accessories', lastAcessoriesUpload ); }
                if (lastMaskUpload) { data.append('upload-mask', lastMaskUpload ); }
                if (lastLeftUpload) { data.append('upload-left', lastLeftUpload ); }
                if (lastRightUpload) { data.append('upload-right', lastRightUpload ); }
                if (lastShoeUpload) { data.append('upload-shoe', lastShoeUpload ); }
                if (lastBottomUpload) { data.append('upload-bottom', lastBottomUpload ); }
    
                data.append('data', JSON.stringify(queryDict))
                this.props.actions.uploadBody(data);

                this.setState({ metaWardenBodyValue: v, isBodyUpload: false });
            }
            else{
                let src = `${url}${Object.keys(queryDict).map(key => key + '=' + queryDict[key]).join('&')}`;
                this.setState({ metaWardenBodyValue: v, src: src, isBodyUpload: false });
            }
        }
        else if (v.value === 'upload'){
            this.setState({ metaWardenBodyValue: v, isBodyUpload: true })
        }
    }

    metaWardenAccessoriesChange = (e,v,r) => {
        //if don't have ID return false
        if (!this.state.metaWardenIDValue) {
            this.setState({isSnackbarOpen: true, warningText: "Choose MetaWarden ID first!" });
            return;
        }

        this.setState({isLoadingImage: true})

        if (v.value !== 'upload') { 
            queryDict["accessories"] = v.value;
            if (v.value === null || v.value === 'none') { delete queryDict["accessories"]; }
            if ( (this.state.isBodyUpload === true && lastBodyUpload) || (this.state.isHeadUpload === true && lastHeadUpload) || (this.state.isBGUpload === true && lastBGUpload) || (this.state.isAccessoriesUpload === true && lastAcessoriesUpload)) {
                //Some component is "upload" selected
                let data = new FormData();
                if (lastBodyUpload) { data.append('upload-body', lastBodyUpload ); }
                if (lastHeadUpload) { data.append('upload-head', lastHeadUpload ); }
                if (lastBGUpload) { data.append('upload-background', lastBGUpload ); }
                if (lastMaskUpload) { data.append('upload-mask', lastMaskUpload ); }
                if (lastLeftUpload) { data.append('upload-left', lastLeftUpload ); }
                if (lastRightUpload) { data.append('upload-right', lastRightUpload ); }
                if (lastShoeUpload) { data.append('upload-shoe', lastShoeUpload ); }
                if (lastBottomUpload) { data.append('upload-bottom', lastBottomUpload ); }
    
                data.append('data', JSON.stringify(queryDict))
                this.props.actions.uploadBody(data);

                this.setState({ metaWardenAccessoriesValue: v, isAccessoriesUpload: false });
            }
            else{
                let src = `${url}${Object.keys(queryDict).map(key => key + '=' + queryDict[key]).join('&')}`;
                this.setState({ metaWardenAccessoriesValue: v, src: src, isAccessoriesUpload: false });
            }
        }
        else if (v.value === 'upload'){
            this.setState({ metaWardenAccessoriesValue: v, isAccessoriesUpload: true })
        }
    }

    metaWardenMaskChange = (e,v,r) => {
        if (!this.state.metaWardenIDValue) {
            this.setState({isSnackbarOpen: true, warningText: "Choose MetaWarden ID first!" });
            return;
        }

        this.setState({isLoadingImage: true})

        if (v.value !== 'upload') { 
            queryDict["mask"] = v.value;
            if (v.value === null || v.value === 'none') { delete queryDict["mask"]; }
            if ( (this.state.isBodyUpload === true && lastBodyUpload) || (this.state.isHeadUpload === true && lastHeadUpload) || (this.state.isBGUpload === true && lastBGUpload) || (this.state.isAccessoriesUpload === true && lastAcessoriesUpload)) {
                //Some component is "upload" selected
                let data = new FormData();
                if (lastBodyUpload) { data.append('upload-body', lastBodyUpload ); }
                if (lastHeadUpload) { data.append('upload-head', lastHeadUpload ); }
                if (lastBGUpload) { data.append('upload-background', lastBGUpload ); }
                if (lastAcessoriesUpload) { data.append('upload-accessories', lastAcessoriesUpload ); }
                if (lastLeftUpload) { data.append('upload-left', lastLeftUpload ); }
                if (lastRightUpload) { data.append('upload-right', lastRightUpload ); }
                if (lastShoeUpload) { data.append('upload-shoe', lastShoeUpload ); }
                if (lastBottomUpload) { data.append('upload-bottom', lastBottomUpload ); }
    

                data.append('data', JSON.stringify(queryDict))
                this.props.actions.uploadBody(data);

                this.setState({ metaWardenMaskValue: v, isMaskUpload: false });
            }
            else{
                let src = `${url}${Object.keys(queryDict).map(key => key + '=' + queryDict[key]).join('&')}`;
                this.setState({ metaWardenMaskValue: v, src: src, isMaskUpload: false });
            }
        }
        else if (v.value === 'upload'){
            this.setState({ metaWardenMaskValue: v, isMaskUpload: true })
        }

    }

    metaWardenStyleChange = (e,v,r) => {
        if (!this.state.metaWardenIDValue) {
            this.setState({isSnackbarOpen: true, warningText: "Choose MetaWarden ID first!" });
            return;
        }

        this.setState({isLoadingImage: true})

        if (v.value !== 'style') { 
            queryDict["style"] = v.value;
            if (v.value === null) { delete queryDict["style"]; }
            if ( (this.state.isBodyUpload === true && lastBodyUpload) || (this.state.isHeadUpload === true && lastHeadUpload) || (this.state.isBGUpload === true && lastBGUpload) || (this.state.isAccessoriesUpload === true && lastAcessoriesUpload)) {
                //Some component is "upload" selected
                let data = new FormData();
                if (lastBodyUpload) { data.append('upload-body', lastBodyUpload ); }
                if (lastHeadUpload) { data.append('upload-head', lastHeadUpload ); }
                if (lastBGUpload) { data.append('upload-background', lastBGUpload ); }
                if (lastAcessoriesUpload) { data.append('upload-accessories', lastAcessoriesUpload ); }
                if (lastMaskUpload) { data.append('upload-mask', lastMaskUpload ); }
                if (lastLeftUpload) { data.append('upload-left', lastLeftUpload ); }
                if (lastRightUpload) { data.append('upload-right', lastRightUpload ); }
                if (lastShoeUpload) { data.append('upload-shoe', lastShoeUpload ); }
                if (lastBottomUpload) { data.append('upload-bottom', lastBottomUpload ); }
    
                data.append('data', JSON.stringify(queryDict))
                this.props.actions.uploadBody(data);

                this.setState({ metaWardenStyleValue: v });
            }
            else{
                let src = `${url}${Object.keys(queryDict).map(key => key + '=' + queryDict[key]).join('&')}`;
                this.setState({ metaWardenStyleValue: v, src: src});
            }
        }
        else if (v.value === 'upload'){
            this.setState({ metaWardenStyleValue: v })
        }

    }

    openParas = () => {
        // window.location.href = "https://paras.id/collection/warden-closet-by-wardenclosetnear";
        window.open("https://paras.id/collection/warden-closet-by-wardenclosetnear","_blank");
    }

    onSnackbarClose = () => {
        this.setState({isSnackbarOpen : false})
    }

    onUploadChange  = (e) => {
        delete queryDict[e.target.id.split("-")[1]]
        let file = e.target.files[0];
        // let mimeType = file.type;
        // let size = file.size; //Byte
        let _URL = window.URL || window.webkitURL;
        // let _metaWardenIDValue = this.state.metaWardenIDValue;
        let _this = this;

        let img = new Image();
        var objectUrl = _URL.createObjectURL(file);

        img.onload = function () {
            // if file size width&height is correct, then upload
            if (uploadXY === this.width && uploadXY === this.height){
                let data = new FormData();

                //if some upload isEnabled and have last data, use last data
                if ( _this.state.isBodyUpload === true && lastBodyUpload ) {  data.append('upload-body', lastBodyUpload); }
                if ( _this.state.isHeadUpload === true && lastHeadUpload ) { data.append('upload-head', lastHeadUpload); }
                if ( _this.state.isBGUpload === true && lastBGUpload ) {  data.append('upload-background', lastBGUpload); }
                if ( _this.state.isAccessoriesUpload === true && lastAcessoriesUpload ) {  data.append('upload-accessories', lastAcessoriesUpload); }
                if ( _this.state.isMaskUpload === true && lastMaskUpload ) {  data.append('upload-mask', lastMaskUpload); }
                if ( _this.state.isLeftUpload === true && lastLeftUpload ) {  data.append('upload-left', lastLeftUpload); }
                if ( _this.state.isRightUpload === true && lastRightUpload ) {  data.append('upload-right', lastRightUpload); }

                //set last data for each input
                if (e.target.id === 'upload-body') {lastBodyUpload = file;}
                if (e.target.id === 'upload-head') {lastHeadUpload = file;}
                if (e.target.id === 'upload-background') {lastBGUpload = file;}
                if (e.target.id === 'upload-accessories') {lastAcessoriesUpload = file;}
                if (e.target.id === 'upload-mask') {lastMaskUpload = file;}
                if (e.target.id === 'upload-left') {lastLeftUpload = file;}
                if (e.target.id === 'upload-right') {lastRightUpload = file;}

                data.delete(e.target.id);
                data.append(e.target.id, file )

                data.append('data', JSON.stringify(queryDict))
                _this.props.actions.uploadBody(data);
            }
            else{
                _this.setState({isSnackbarOpen: true, warningText: "Image dimansion must be 1024x1024px" });
            }//end if
        };
        img.src = objectUrl;

    }

    render(){
        return (
            <Grid container justifyContent="center" alignItems="center" sx={{ height: '95vh' }}   >
                
                <Grid container className='Main-VCenter' md={12} lg={12} xl={8}>
                    
                    <Grid item xs={12} sm={12} md={12} lg={6} xl={6} >
                        <img className='Main-logo' src={require('../assets/img/logo-closet-whitebg.png')} alt="logo" />
                    
                        <Autocomplete
                            disablePortal
                            disableClearable
                            className='Main-autocomplete'
                            id="combo-metaWarden-mode"
                            value={this.state.metaWardenModeValue}
                            options={this.state.metaWardenModeList}
                            renderInput={(params) => <TextField {...params} label="Display Mode" size="small" />}
                            onChange={this.metaWardenModeChange}
                        />

                        <Autocomplete
                            disablePortal
                            disableClearable
                            className='Main-autocomplete'
                            id="combo-metaWarden-Id"
                            value={this.state.metaWardenIDValue}
                            options={this.state.metaWardenIDList}
                            renderInput={(params) => <TextField {...params} label="MetaWarden ID" size="small" />}
                            onChange={this.metaWardenIDChange}
                        />

                        {
                            this.state.metaWardenModeValue.value !== 'back' && 
                            <div>
                                <Autocomplete
                                    disablePortal
                                    disableClearable
                                    className='Main-autocomplete'
                                    id="combo-metaWarden-Background"
                                    value={this.state.metaWardenBGValue}
                                    options={this.state.metaWardenBGList}
                                    renderInput={(params) => <TextField {...params} label="Background" size="small"/>}
                                    onChange={this.metaWardenBGChange}
                                />
                                {
                                    this.state.isBGUpload &&
                                    <div className='Main-UploadBtn'>
                                        <label htmlFor="upload-background">
                                            <Input accept="image/png" id="upload-background" multiple type="file" onChange={this.onUploadChange}/>
                                            <Button variant="contained" component="span" color="secondary" >
                                                Upload Background
                                            </Button>
                                        </label>
                                        <div className='Main-DownloadClothingTamplate'>
                                            { `Download clothing template here! `}
                                            <a href={`${IMAGE_HOST}/rest/images/bg_template.png`} target="_blank" rel="noreferrer">
                                            PNG
                                            </a>
                                        </div>
                                    </div>
                                }

                                <Autocomplete
                                    disablePortal
                                    disableClearable
                                    className='Main-autocomplete'
                                    id="combo-metaWarden-Head"
                                    value={this.state.metaWardenHeadValue}
                                    options={this.state.metaWardenHeadList}
                                    renderInput={(params) => <TextField {...params} label="Headware" size="small"/>}
                                    onChange={this.metaWardenHeadChange}
                                />
                                {
                                    this.state.isHeadUpload &&
                                    <div className='Main-UploadBtn'>
                                        <label htmlFor="upload-head">
                                            <Input accept="image/png" id="upload-head" multiple type="file" onChange={this.onUploadChange}/>
                                            <Button variant="contained" component="span" color="secondary" >
                                                Upload Headware
                                            </Button>
                                        </label>
                                        <div className='Main-DownloadClothingTamplate'>
                                            { `Download clothing template here! `}
                                            <a href={`${IMAGE_HOST}/rest/images/head_template.png`} target="_blank" rel="noreferrer">
                                            PNG
                                            </a>
                                        </div>
                                    </div>
                                }

                                <Autocomplete
                                    disablePortal
                                    disableClearable
                                    className='Main-autocomplete'
                                    id="combo-metaWarden-Accessories"
                                    value={this.state.metaWardenAccessoriesValue}
                                    options={this.state.metaWardenAccessoriesList}
                                    renderInput={(params) => <TextField {...params} label="Accessories" size="small"/>}
                                    onChange={this.metaWardenAccessoriesChange}
                                />
                                {
                                    this.state.isAccessoriesUpload &&
                                    <div className='Main-UploadBtn'>
                                        <label htmlFor="upload-accessories">
                                            <Input accept="image/png" id="upload-accessories" multiple type="file" onChange={this.onUploadChange}/>
                                            <Button variant="contained" component="span" color="secondary" >
                                                Upload Accessories
                                            </Button>
                                        </label>
                                        <div className='Main-DownloadClothingTamplate'>
                                            { `Download clothing template here! `}
                                            <a href={`${IMAGE_HOST}/rest/images/accessories_template.png`} target="_blank" rel="noreferrer">
                                            PNG
                                            </a>
                                        </div>
                                    </div>
                                }
                                
                                <Autocomplete
                                    disablePortal
                                    disableClearable
                                    className='Main-autocomplete'
                                    id="combo-metaWarden-Mask"
                                    value={this.state.metaWardenMaskValue}
                                    options={this.state.metaWardenMaskList}
                                    renderInput={(params) => <TextField {...params} label="Mask" size="small"/>}
                                    onChange={this.metaWardenMaskChange}
                                />

                                {
                                    this.state.isMaskUpload &&
                                    <div className='Main-UploadBtn'>
                                        <label htmlFor="upload-mask">
                                            <Input accept="image/png" id="upload-mask" multiple type="file" onChange={this.onUploadChange}/>
                                            <Button variant="contained" component="span" color="secondary" >
                                                Upload Mask
                                            </Button>
                                        </label>
                                        <div className='Main-DownloadClothingTamplate'>
                                            { `Download clothing template here! `}
                                            <a href={`${IMAGE_HOST}/rest/images/common_template.png`} target="_blank" rel="noreferrer">
                                            PNG
                                            </a>
                                        </div>
                                    </div>
                                }

                                <Autocomplete
                                    disablePortal
                                    disableClearable
                                    className='Main-autocomplete'
                                    id="combo-metaWarden-Body"
                                    value={this.state.metaWardenBodyValue}
                                    // getOptionLabel={option => option.label}
                                    options={this.state.metaWardenBodyList}
                                    renderInput={(params) => <TextField {...params} label="Clothing" size="small"/>}
                                    onChange={this.metaWardenBodyChange}
                                />
                                {
                                    this.state.isBodyUpload &&
                                    <div className='Main-UploadBtn'>
                                        <label htmlFor="upload-body">
                                            <Input accept="image/png" id="upload-body" multiple type="file" onChange={this.onUploadChange}/>
                                            <Button variant="contained" component="span" color="secondary" >
                                                Upload Clothing
                                            </Button>
                                        </label>
                                        <div className='Main-DownloadClothingTamplate'>
                                            { `Download clothing template here! `}
                                            <a href={`${IMAGE_HOST}/rest/images/common_template.png`} target="_blank" rel="noreferrer">
                                            PNG
                                            </a>
                                            { ` `}
                                            <a href={`${IMAGE_HOST}/rest/images/WardenClosetBasicTemplate.zip`} target="_blank" rel="noreferrer">
                                            PSD
                                            </a>
                                        </div>
                                    </div>
                                }

                                <Autocomplete
                                    disablePortal
                                    disableClearable
                                    className='Main-autocomplete'
                                    id="combo-metaWarden-Styling"
                                    value={this.state.metaWardenStyleValue}
                                    options={this.state.metaWardenStyleList}
                                    renderInput={(params) => <TextField {...params} label="Styling" size="small"/>}
                                    onChange={this.metaWardenStyleChange}
                                />

                            </div>
                        }
                        

                        {
                            this.state.metaWardenModeValue.value === 'full' && 
                            <div>
                                <Autocomplete
                                    disablePortal
                                    disableClearable
                                    className='Main-autocomplete'
                                    id="combo-metaWarden-Left"
                                    value={this.state.metaWardenLeftValue}
                                    options={this.state.metaWardenLeftList}
                                    renderInput={(params) => <TextField {...params} label="Left hand of MWAD" size="small"/>}
                                    onChange={this.metaWardenLeftChange}
                                />
                                {
                                    this.state.isLeftUpload &&
                                    <div className='Main-UploadBtn'>
                                        <label htmlFor="upload-left">
                                            <Input accept="image/png" id="upload-left" multiple type="file" onChange={this.onUploadChange}/>
                                            <Button variant="contained" component="span" color="secondary" >
                                                Upload Left hand
                                            </Button>
                                        </label>
                                        <div className='Main-DownloadClothingTamplate'>
                                            { `Download clothing template here! `}
                                            <a href={`${IMAGE_HOST}/rest/images/common_template.png`} target="_blank" rel="noreferrer">
                                            PNG
                                            </a>
                                        </div>
                                    </div>
                                }

                                <Autocomplete
                                    disablePortal
                                    disableClearable
                                    className='Main-autocomplete'
                                    id="combo-metaWarden-Right"
                                    value={this.state.metaWardenRightValue}
                                    options={this.state.metaWardenRightList}
                                    renderInput={(params) => <TextField {...params} label="Right hand of MWAD" size="small"/>}
                                    onChange={this.metaWardenRightChange}
                                />
                                {
                                    this.state.isRightUpload &&
                                    <div className='Main-UploadBtn'>
                                        <label htmlFor="upload-right">
                                            <Input accept="image/png" id="upload-right" multiple type="file" onChange={this.onUploadChange}/>
                                            <Button variant="contained" component="span" color="secondary" >
                                                Upload Right hand
                                            </Button>
                                        </label>
                                        <div className='Main-DownloadClothingTamplate'>
                                            { `Download clothing template here! `}
                                            <a href={`${IMAGE_HOST}/rest/images/common_template.png`} target="_blank" rel="noreferrer">
                                            PNG
                                            </a>
                                        </div>
                                    </div>
                                }

                                <Autocomplete
                                    disablePortal
                                    disableClearable
                                    className='Main-autocomplete'
                                    id="combo-metaWarden-Trousers"
                                    value={this.state.metaWardenTrousersValue}
                                    options={this.state.metaWardenTrousersList}
                                    renderInput={(params) => <TextField {...params} label="Trousers/Skirt" size="small"/>}
                                    onChange={this.metaWardenTrousersChange}
                                />
                                {
                                    this.state.isBottomUpload &&
                                    <div className='Main-UploadBtn'>
                                        <label htmlFor="upload-bottom">
                                            <Input accept="image/png" id="upload-bottom" multiple type="file" onChange={this.onUploadChange}/>
                                            <Button variant="contained" component="span" color="secondary" >
                                                Upload Trouser
                                            </Button>
                                        </label>
                                        <div className='Main-DownloadClothingTamplate'>
                                            { `Download clothing template here! `}
                                            <a href={`${IMAGE_HOST}/rest/images/common_template.png`} target="_blank" rel="noreferrer">
                                            PNG
                                            </a>
                                        </div>
                                    </div>
                                }

                                <Autocomplete
                                    disablePortal
                                    disableClearable
                                    className='Main-autocomplete'
                                    id="combo-metaWarden-Shoe"
                                    value={this.state.metaWardenShoeValue}
                                    options={this.state.metaWardenShoeList}
                                    renderInput={(params) => <TextField {...params} label="Shoe" size="small"/>}
                                    onChange={this.metaWardenShoeChange}
                                />

                                {
                                    this.state.isShoeUpload &&
                                    <div className='Main-UploadBtn'>
                                        <label htmlFor="upload-shoe">
                                            <Input accept="image/png" id="upload-shoe" multiple type="file" onChange={this.onUploadChange}/>
                                            <Button variant="contained" component="span" color="secondary" >
                                                Upload Trouser
                                            </Button>
                                        </label>
                                        <div className='Main-DownloadClothingTamplate'>
                                            { `Download clothing template here! `}
                                            <a href={`${IMAGE_HOST}/rest/images/common_template.png`} target="_blank" rel="noreferrer">
                                            PNG
                                            </a>
                                        </div>
                                    </div>
                                }

                                <Autocomplete
                                    disablePortal
                                    disableClearable
                                    className='Main-autocomplete'
                                    id="combo-metaWarden-Car"
                                    value={this.state.metaWardenCarValue}
                                    options={this.state.metaWardenCarList}
                                    renderInput={(params) => <TextField {...params} label="Car" size="small"/>}
                                    onChange={this.metaWardenCarChange}
                                />

                            </div>
                        }
                        
                        {
                        this.state.metaWardenModeValue.value === 'back' && 
                        <div>
                            <Autocomplete
                                disablePortal
                                disableClearable
                                className='Main-autocomplete'
                                id="combo-metaWarden-Bag"
                                value={this.state.metaWardenBagValue}
                                options={this.state.metaWardenBagList}
                                renderInput={(params) => <TextField {...params} label="Bag" size="small"/>}
                                onChange={this.metaWardenBagChange}
                            />
                        </div>
                        }

                        <div className='Main-shareTitle'>
                            <Button variant="contained" component="span" onClick={this.openParas}  >
                                Mint Clothing
                            </Button>
                        </div>
                        
                        
                        
                        <Grid item xs={12} sm={12} md={12} sx={{padding:"7px"}} >
                        
                            <div className='Main-shareTitle'>
                                Enjoying Warden closet? Please share about us!
                            </div>

                            <div className='Main-shareIcon-container'>
                                <span className='Main-shareIcon' >
                                    <TwitterShareButton 
                                        url={`${window.location.href}`}

                                        hashtags={["MetaWardenNFT", "MetaWarden", "MetaWardenFanArt", "MetaWardenBrand"]}
                                        title='Something really cool!'>
                                            <TwitterIcon
                                            size={32}
                                            round />
                                    </TwitterShareButton>
                                </span>

                            </div>

                        </Grid>
                    </Grid>

                    <Grid item xs={12} sm={12} md={12} lg={6} xl={6} sx={{display: "inherit"}}>
                        <img 
                            src={this.state.src}
                            style={{maxWidth:"100%", display: "flex", opacity: this.state.isLoadingImage ? '0.7': '1'}}
                            className="Main-targetImage"
                            alt="result"
                            onLoad={() => this.setState({isLoadingImage: false})}
                        />
                    </Grid>

                    <div className='Main-Credit'>
                    <div >Made by <a href="https://twitter.com/PPHCYWS" target="_blank" rel="noreferrer">@PPHCYWS</a>  with designs from <a href="https://twitter.com/SPY_dGs" target="_blank" rel="noreferrer">@SPY_dGs</a>, <a href="https://twitter.com/paperontwit" target="_blank" rel="noreferrer">@paperontwit</a><br/> and thanks to <a href="https://twitter.com/MissionSaturn" target="_blank" rel="noreferrer">@MissionSaturn</a>, <a href="https://twitter.com/PanasL27" target="_blank" rel="noreferrer">@PanasL27</a> for leading this community</div>

                    <div className='Main-tag'>#MetaWardenNFT #MetaWarden #MetaWardenFanArt #MetaWardenBrand</div> 

                    <div className='Main-Donate'>Donate for @PPHCYWS (BSC) : 0x11f70FbB466DCEBd7d9596Ee27bFcDe676fD8239  </div> 
                    <div className='Main-Donate2'>Donate for Metawarden Village : 0x27E958E572ed5E759CD8DEBBbF28645C66331556  </div> 
                    </div>
 

                </Grid>

                <Snackbar open={this.state.isSnackbarOpen} autoHideDuration={3000}  onClose={this.onSnackbarClose} anchorOrigin={{ vertical: "top", horizontal: "center" }} >
                    <Alert  severity="warning" sx={{ width: '100%' }}>
                        
                        {this.state.warningText}
                    </Alert>
                </Snackbar>
            </Grid>  
        )
    }
}

function mapStateToProps(state){
    return {

        uploadBody: state.uploadBody
    }
}

function mapDispatchToProps(dispatch){
  return {
    UPLOADBODY_RESET: () => {
      dispatch({type: 'UPLOADBODY_RESET', payload: null})
    },

    actions: bindActionCreators(actions, dispatch),
  }
}

export default connect(mapStateToProps,mapDispatchToProps) (Main) 